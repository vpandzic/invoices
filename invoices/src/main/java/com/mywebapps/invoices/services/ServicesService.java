package com.mywebapps.invoices.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mywebapps.invoices.models.*;
import com.mywebapps.invoices.repositories.*;

@Service
public class ServicesService{

    @Autowired
    private ServicesRepository servicesRepo;

     public List<ServiceModel> getAll(){
        return servicesRepo.findAll();
    }
    public List<ServiceModel> findByUserId(Long id){
        return servicesRepo.findByUserId(id);
    }
   
    public Optional<ServiceModel> getById(Long id){
        return servicesRepo.findById(id);
    }
    public ServiceModel insert(ServiceModel product){
        return servicesRepo.save(product);
    }
    public void delete(Long id){
        servicesRepo.deleteById(id);
    }
    public void update(ServiceModel product){
        servicesRepo.save(product);
    }
}