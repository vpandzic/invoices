package com.mywebapps.invoices.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mywebapps.invoices.models.*;
import com.mywebapps.invoices.repositories.*;

@Service
public class InvoicesService{

    @Autowired
    private InvoicesRepository invoicesRepo;

     public List<Invoice> getAll(){
        return invoicesRepo.findAll();
    }
    public Optional<Invoice> getById(Long id){
        return invoicesRepo.findById(id);
    }
    public List<Invoice> findByUserId(Long id){
        return invoicesRepo.findByUserId(id);
    }
    public List<Invoice> findByUserIdAndServiceId(Long id,Long serviceId){
        return invoicesRepo.findByUserIdAndServiceId(id,serviceId);
    }
    public Invoice insert(Invoice product){
        return invoicesRepo.save(product);
    }
    public void delete(Long id){
        invoicesRepo.deleteById(id);
    }
    public void update(Invoice product){
        invoicesRepo.save(product);
    }
}