package com.mywebapps.invoices.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

import com.mywebapps.invoices.models.UserModel;
import com.mywebapps.invoices.repositories.*;

@Service
public class UsersService {

    @Autowired
    private UsersRepository userRepo;

    public List<UserModel> findByEmailAndPassword(String email, String password){
        return userRepo.findByEmailAndPassword(email,password);
    }
    public List<UserModel> findByEmail(String email){
        return userRepo.findByEmail(email);
    }
   
    public Optional<UserModel> getById(Long id){
        return userRepo.findById(id);
    }
    public UserModel insert(UserModel product){

        return userRepo.save(product);
    }
    public void update(UserModel product){
        userRepo.save(product);
    }
}
