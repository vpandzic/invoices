package com.mywebapps.invoices.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Data
@AllArgsConstructor
@Table(name = "services")
@Entity
public class ServiceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Long getUser_id() {
        return userid;
    }

    public void setUser_id(Long user_id) {
        this.userid = user_id;
    }

   
   
    @Column(name = "user_id",nullable = true)
    private Long userid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id",updatable = false,insertable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private UserModel user;
}
