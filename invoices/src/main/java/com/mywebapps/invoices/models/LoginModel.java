package com.mywebapps.invoices.models;

import javax.validation.constraints.NotNull;

public class LoginModel {

    @NotNull
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @NotNull
    private String password;
}
