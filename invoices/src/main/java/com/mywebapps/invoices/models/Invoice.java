package com.mywebapps.invoices.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Data
@AllArgsConstructor
@Table(name = "invoices")
@Entity
 public class Invoice {

    public Invoice(){

    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private Boolean status;

    @NotNull
    private BigDecimal price;
    

    @NotNull
    private String name;

    @NotNull
    private Integer month;

    @NotNull
    private Integer year;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public ServiceModel getService() {
        return service;
    }

    public void setCategory(ServiceModel service) {
        this.service = service;
    }

    public Long getService_id() {
        return serviceid;
    }

    public void setService_id(Long service_id) {
        this.serviceid = service_id;
    }
    public Long getUser_id() {
        return userid;
    }

    public void setUser_id(Long user_id) {
        this.userid = user_id;
    }

    @NotNull
    @Column(name = "service_id",nullable = true)
    private Long serviceid;

   
    @Column(name = "user_id",nullable = true)
    private Long userid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="service_id",updatable = false,insertable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private ServiceModel service;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id",updatable = false,insertable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private UserModel user;


   
 }