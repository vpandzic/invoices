package com.mywebapps.invoices.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/home","/**/users/login").permitAll()
                .antMatchers("/", "/home","/**/users/register").permitAll()
                // .antMatchers(HttpMethod.POST,"/**/users/**").permitAll()
                // .antMatchers(HttpMethod.PUT,"/**/users/**").permitAll()
                // .antMatchers(HttpMethod.GET,"/**/users/current").denyAll()
                .anyRequest().authenticated()
                .and().cors().and().authenticationProvider(new CustomAuthenticationProvider(getApplicationContext()))
//                .logout()
//                .logoutUrl("/api/users/logout")
//                .invalidateHttpSession(true)
//                .deleteCookies("JSESSIONID").logoutSuccessUrl("/api/products")
//                .permitAll()
//                .and()
                .cors().and().csrf().disable();

    }
    @Autowired

    private CustomAuthenticationProvider authProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);

    }
    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


}
