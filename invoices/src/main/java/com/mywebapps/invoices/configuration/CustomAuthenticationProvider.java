package com.mywebapps.invoices.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import com.mywebapps.invoices.services.UsersService;

@Component

public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final UsersService userService;

    CustomAuthenticationProvider(ApplicationContext ctx){
        this.userService= ctx.getBean(UsersService.class);
    }

    @Override

    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (userService.findByEmailAndPassword(name,password).isEmpty()==false) {


            return new UsernamePasswordAuthenticationToken(
                    name, password, new ArrayList<>());
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}