package com.mywebapps.invoices.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.ws.ServiceMode;

import com.mywebapps.invoices.models.Invoice;
import com.mywebapps.invoices.models.ServiceModel;
import com.mywebapps.invoices.models.UserModel;
import com.mywebapps.invoices.services.InvoicesService;
import com.mywebapps.invoices.services.ServicesService;
import com.mywebapps.invoices.services.UsersService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/services")
@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
public class ServicesController {

    @Autowired
    private ServicesService service;

    @Autowired
    private UsersService userService;

    @Autowired
    private InvoicesService invoicesService;

    @RequestMapping(value = {"","/"},method = RequestMethod.GET)
    @GetMapping
    public List<ServiceModel> Services(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserModel> userModel=userService.findByEmail(principal.toString());
        List<ServiceModel> services= service.findByUserId(userModel.get(0).getId());
        return services;
    }
   
    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity GetOneService(@PathVariable Long id){

        Optional<ServiceModel> c= service.getById(id);
        if(c.isPresent()){

            return new ResponseEntity(c, HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = {"","/"},method = RequestMethod.POST,consumes = "application/json",produces="application/json")
    public ResponseEntity PostService(@RequestBody @Valid ServiceModel c, BindingResult result){

        if(result.hasErrors()==false) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<UserModel> userModel=userService.findByEmail(principal.toString());
            c.setUser_id(userModel.get(0).getId());
            return new ResponseEntity<>(service.insert(c), HttpStatus.CREATED);
        }
        else
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.PUT)
    public ResponseEntity Update(@PathVariable Long id,@RequestBody @Valid ServiceModel serviceModelDto,BindingResult result){

        if(result.hasErrors()==false) {
            Optional<ServiceModel> p = service.getById(id);
            if(p.isPresent()){
                serviceModelDto.setId(id);
                service.update(serviceModelDto);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        }else
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.DELETE)
    public ResponseEntity Delete(@PathVariable Long id){
        try {
            service.delete(id);
            return  new ResponseEntity(HttpStatus.OK);
        }catch (Exception ex){
            return  new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @RequestMapping(value = {"{id}/invoices"},method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity InvoicesByServiceId(@PathVariable Long id,final HttpServletRequest request, final HttpSession session){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserModel> userModel=userService.findByEmail(principal.toString());
        List<Invoice> invoices= invoicesService.findByUserIdAndServiceId(userModel.get(0).getId(),id);
       // final List<Invoice> invoices= service.getAll();
        return new ResponseEntity(invoices,HttpStatus.OK);
    }

}