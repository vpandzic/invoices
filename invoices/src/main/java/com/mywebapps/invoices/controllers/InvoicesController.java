package com.mywebapps.invoices.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mywebapps.invoices.models.UserModel;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.mywebapps.invoices.models.Invoice;
import com.mywebapps.invoices.services.*;
import java.util.Optional;

@RestController
@RequestMapping("/api/invoices")
public class InvoicesController {


    @Autowired
    private InvoicesService service;

    @Autowired
    private UsersService userService;

    @RequestMapping(value = {"","/"},method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity Invoices(final HttpServletRequest request, final HttpSession session){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<UserModel> userModel=userService.findByEmail(principal.toString());
        List<Invoice> invoices= service.findByUserId(userModel.get(0).getId());
       // final List<Invoice> invoices= service.getAll();
        return new ResponseEntity(invoices,HttpStatus.OK);
    }
    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity GetOneInvoice(@PathVariable final Long id){

        Optional<Invoice> p= service.getById(id);
        if(p.isPresent()){

            return new ResponseEntity(p,HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = {"","/"},method = RequestMethod.POST,consumes = "application/json",produces="application/json")
    public ResponseEntity PostProduct(@RequestBody @Valid final Invoice p, final BindingResult result, final HttpSession session){

        if(result.hasErrors()==false) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<UserModel> userModel=userService.findByEmail(principal.toString());
            p.setUser_id(userModel.get(0).getId());
            return new ResponseEntity<>(service.insert(p), HttpStatus.CREATED);
        }
       else
           return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.PUT)
    @PutMapping
    public ResponseEntity Update(@PathVariable final Long id,@RequestBody @Valid final Invoice invoiceDto,final BindingResult result){

        if(result.hasErrors()==false) {
            Optional<Invoice> p = service.getById(id);
            if(p.isPresent()){
                invoiceDto.setId(id);
                 service.update(invoiceDto);
                 return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        }else
          return  new ResponseEntity(HttpStatus.BAD_REQUEST);

    }


    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.DELETE)
    public ResponseEntity Delete(@PathVariable final Long id){
        try {
            service.delete(id);
            return  new ResponseEntity(HttpStatus.OK);
        }catch (final Exception ex){
            return  new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
   
}