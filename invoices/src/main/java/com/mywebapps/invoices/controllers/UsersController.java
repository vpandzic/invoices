package com.mywebapps.invoices.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.mywebapps.invoices.models.LoginModel;
import com.mywebapps.invoices.models.UserModel;
import com.mywebapps.invoices.services.UsersService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
public class UsersController {

    @Autowired
    AuthenticationManager authManager;

    @Autowired
    private UsersService service;

    @PostMapping
    @RequestMapping(value = {"login"},method = RequestMethod.POST)
    public ResponseEntity Login(@RequestBody @Valid LoginModel user, HttpServletRequest request){

        List<UserModel> userFromDb=service.findByEmailAndPassword(user.getEmail(),user.getPassword());
        if(!userFromDb.isEmpty()){
            UsernamePasswordAuthenticationToken authReq
                    = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
            Authentication auth = authManager.authenticate(authReq);
           

            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(auth);
        
            HttpSession session = request.getSession(true);
            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }
    @GetMapping
    @RequestMapping(value = {"logout"},method = RequestMethod.GET)
    public ResponseEntity Logout(HttpServletRequest request, SessionStatus status,HttpSession session, HttpServletResponse response){

        session.invalidate();

        Cookie[] cookies = request.getCookies();
        if(cookies!=null) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    @PostMapping
    @RequestMapping(value = {"register"},method = RequestMethod.POST)
    public ResponseEntity Register(@RequestBody @Valid UserModel user, BindingResult bindingResult,HttpServletRequest request){

        if(bindingResult.hasErrors()==false) {

            List<UserModel> c= service.findByEmail(user.getEmail());
            if(c.size()>0){
                return  new ResponseEntity("Email already exists",HttpStatus.BAD_REQUEST);
            }

            ResponseEntity response= new ResponseEntity<>(service.insert(user), HttpStatus.CREATED);
            UsernamePasswordAuthenticationToken authReq
            = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
            Authentication auth = authManager.authenticate(authReq);

            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(auth);
            HttpSession session = request.getSession(true);
            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
            return response;
        }
        else
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = {"current"},method = RequestMethod.GET)
    @GetMapping
    public ResponseEntity GetOneUser(){


        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
      
        List<UserModel> c= service.findByEmail(principal.toString());
        if(c.size()==1){

            return new ResponseEntity(c, HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @RequestMapping(value = {"","/"},method = RequestMethod.POST,consumes = "application/json",produces="application/json")
    public ResponseEntity PostService(@RequestBody @Valid UserModel c, BindingResult result){

        if(result.hasErrors()==false) {

            return new ResponseEntity<>(service.insert(c), HttpStatus.CREATED);
        }
        else
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    @RequestMapping(value = {"{id}","/{id}"},method = RequestMethod.PUT)
    public ResponseEntity Update(@PathVariable Long id,@RequestBody @Valid UserModel serviceModelDto,BindingResult result){

        if(result.hasErrors()==false) {
            Optional<UserModel> p = service.getById(id);
            if(p.isPresent()){
                serviceModelDto.setId(id);
                service.update(serviceModelDto);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        }else
            return  new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

}
