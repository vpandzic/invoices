package com.mywebapps.invoices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.mywebapps.invoices.models")
@ComponentScan("com.mywebapps.invoices.services")
@ComponentScan("ccom.mywebapps.invoices.controllers")
@EnableJpaRepositories(basePackages = "com.mywebapps.invoices.repositories")
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
