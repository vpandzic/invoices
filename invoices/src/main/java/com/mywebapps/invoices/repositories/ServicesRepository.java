package com.mywebapps.invoices.repositories;



import java.util.List;

import com.mywebapps.invoices.models.Invoice;
import com.mywebapps.invoices.models.ServiceModel;
import com.mywebapps.invoices.models.UserModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ServicesRepository extends JpaRepository<ServiceModel,Long> {

    List<ServiceModel> findByUserId(Long id);
}
