package com.mywebapps.invoices.repositories;

import java.util.List;

import com.mywebapps.invoices.models.Invoice;

import org.springframework.data.jpa.repository.JpaRepository;



public interface InvoicesRepository extends JpaRepository<Invoice,Long> {
    List<Invoice> findByUserId(Long id);
    List<Invoice> findByUserIdAndServiceId(Long id,Long userId);
}
