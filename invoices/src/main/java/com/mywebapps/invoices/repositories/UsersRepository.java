package com.mywebapps.invoices.repositories;

import java.util.List;

import com.mywebapps.invoices.models.Invoice;
import com.mywebapps.invoices.models.UserModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<UserModel,Long> {
    List<UserModel> findByEmailAndPassword(String email, String password);
    List<UserModel> findByEmail(String email);
}
