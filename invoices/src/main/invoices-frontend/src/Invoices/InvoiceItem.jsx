import React, { Component } from 'react';
import { Link } from 'react-router-dom'

export default class InvoiceItem extends Component {


    render(){
        const invoice=this.props.invoice;
        return (
         
            <div className="card" >
                   <span className="badge badge-success badge-small">{invoice.service && invoice.service.name}</span> 
                <img className="card-img-top" src="https://via.placeholder.com/150" alt="Card image cap" />
                <div className="card-body">
                    <h5 className="card-title">{invoice.name}  <span class="badge badge-warning">{invoice.price} kn</span></h5>
                    <div className="card-text">{invoice.price}</div>
                   
                    <Link to={"/invoices/"+invoice.id}  params={{ invoice: invoice}} className="btn btn-primary">Detaljno</Link>
                </div>
          </div>
       )
    }
}
