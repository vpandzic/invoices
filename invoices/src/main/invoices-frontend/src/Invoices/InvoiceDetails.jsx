import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import dataService from '../dataService';
export default class InvoiceDetails extends Component {

   constructor(props){
       super(props);
       this.state={
           invoice:{}
       }
       this.deleteInvoice=this.deleteInvoice.bind(this);
   }
   deleteInvoice(){
     dataService.deleteInvoice(this.props.match.params.id).then((response)=>{
                if(response.status==200){
                      window.location.href="/invoices/";
                }else{
                    alert("Došlo je do greške")
                }
        })
       
   }
   componentDidMount() {
    this.setState({isLoading: true});
 
    dataService.getOneInvoice(this.props.match.params.id)
      .then(data =>{
         
          this.setState({invoice: data, isLoading: false})
          }
      );
  }
   render(){
       const {invoice}=this.state;
       return (
           <div className="col-md-12">
               <br/>
               <div className="row">
                    <div className="col-md-4">
                        <img className="card-img-top" src="https://via.placeholder.com/150" alt="Card image cap" />
                    </div>
                    <div className="col-md-6">
                        <h2>{invoice.name}</h2>
                        <h4>{invoice.price} kn</h4>
                        <h6><span className="badge badge-success">{invoice.service && invoice.service.name}</span></h6>
                        <p>{invoice.price}</p>
                        <hr/>
                        <div class="btn-group">
                            <Link role="button"   className="btn btn-warning" to={"/invoices/"+invoice.id+"/addEdit"}>Uredi</Link>
                            <button className="btn btn-danger" onClick={this.deleteInvoice}>Izbriši</button>
                       </div>
                    </div>                
               </div>            
           </div>
       )
   }
}