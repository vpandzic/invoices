import React, { Component } from 'react';
import InvoiceItem from './InvoiceItem'
import { Link } from 'react-router-dom'
import dataService from '../dataService';

export default class InvoiceList extends Component {

  constructor(props) {
    super(props);
    this.state = {
         invoices: [],
         isLoading: true
        };
  
  }

  componentDidMount() {
    this.setState({isLoading: true});

    dataService.getInvoices()
      .then(data =>{ 
          this.setState({invoices: data, isLoading: false})
          }
      );
  }
  render(){
        return (<div class="col-md-12">
            <br/>
            <h2>Računi</h2>
            <hr/>
             <Link to="/invoices/-1/addEdit" className="btn btn-primary">Dodaj novi račun</Link>
            <hr/>
            <div className="row">
                {this.state.invoices.map((invoice)=>{
                    return (
                        <div className="col-md-3">
                            <InvoiceItem invoice={invoice} />
                    </div>
                    )
                })
                }
           </div>
         </div>
        )
  }
}
