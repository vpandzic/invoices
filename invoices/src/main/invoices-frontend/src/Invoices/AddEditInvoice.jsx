import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Formik ,ErrorMessage, Field } from 'formik';
import * as yup from 'yup'
import FormInput from '../Common/FormInput'
import FormSelect from '../Common/FormSelect'
import dataService from '../dataService';
export default class AddEditInvoice extends Component {
    
constructor(props){
    super(props)
    this.state={
        invoice:{
            name:null,
            serviceid:"",
            price:null,
        },
        isNewInvoice:this.props.match.params.id=="-1",
        invoiceid:this.props.match.params.id,
        isSaving:false,
        saved:false,
        services:[{value:"",name:"Odaberi uslugu"}]
    }
  
}
handleResponse=(response)=>{
   
         
    if(response.status==201 || response.status==200){
        this.setState({
            saved:true,
            isSaving:false,
            invoice:{}
        })
    }
    else{
        this.setState({
            error:true,
            isSaving:false
        })
    }  
}
componentDidMount(){
  dataService.getServices()
    .then(data =>{
          const services=data.map(c=>{return {value:c.id,name:c.name}})
          this.setState({
              services:[...this.state.services,...services]
          })  
    });

    if(!this.state.isNewInvoice){
      dataService.getOneInvoice(this.state.invoiceid).then((data)=>{
            
            this.setState({
                invoice:data
            })
        })
        }
}
render(){
    const {isNewInvoice}=this.state;
    return (<div>
        <br/>
        <h2>{isNewInvoice ? "Unesi novi račun ":"Uredi račun "}</h2>
       {this.state.saved && <div class="alert alert-success">Uspješno spremljeno</div> }
       {this.state.error && <div class="alert alert-danger">Došlo je do greške</div> }
        <hr/>
     <Formik
     enableReinitialize={true} 
      initialValues={this.state.invoice}
       onSubmit={(values, actions) => {
           this.setState({
               isSaving:true,
               saved:false,
               error:false
           })
         
           if(this.state.isNewInvoice==false){
            delete values["service_id"]
            dataService.updateInvoice(this.state.invoiceId,values).then(this.handleResponse)
          
           }else{

             dataService.insertInvoice(values).then(this.handleResponse)
           }
        
       
      }}
      
      validationSchema={
          yup.object().shape({
                name: yup.string().nullable().required("Ime je obavezno polje"),
                price: yup.number().nullable().typeError("Unesena vrijednost mora biti broj").required("Cijena je obavezno polje"),
                month: yup.number().nullable().typeError("Unesena vrijednost mora biti broj").required("Mjesec je obavezno polje"),
                year: yup.number().nullable().typeError("Unesena vrijednost mora biti broj").required("Godina je obavezno polje"),
                status: yup.boolean(),
                service_id: yup.number().nullable().required("Usluga je obavezno polje")
      })
    }
  
      render={props => (
        <form onSubmit={props.handleSubmit} className="needs-validation">
          
          
           <Field name="name"  render={({field,form}) => (
                <FormInput  label="Naziv:"  name={"name"} field={field} form={form}  />
            )}
          />
          
          <Field name="price"  render={({field,form}) => (
                <FormInput  label="Cijena:"  name={"price"} field={field} form={form}  />
            )}
          />
           <Field name="month"  render={({field,form}) => (
                <FormInput  label="Mjesec:"  name={"month"} field={field} form={form}  />
            )}
          />
           <Field name="year"  render={({field,form}) => (
                <FormInput  label="Godina:"  name={"year"} field={field} form={form}  />
            )}
          />
            <Field name="status"  render={({field,form}) => (
                <FormInput  label="Plaćeno:"  name={"status"} field={field} form={form}  type="checkbox" />
            )}
          />
          <Field name="service_id"  render={({field,form}) => (
                <FormSelect options={this.state.services}  label="Usluga:"  name={"serviceid"} field={field} form={form}  />
            )}
          />
       
         
          <button type="submit" disabled={this.state.isSaving} className="btn btn-primary btn-large" >Spremi</button>
        </form>
       )}
       />
    </div>)
}

}