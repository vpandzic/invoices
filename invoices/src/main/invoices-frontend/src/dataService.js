import axios from 'axios'

export default class DataService{

   static getInvoices=()=>{
       
         return  axios.get('http://localhost:8080/api/invoices',{withCredentials: true}).then(response=>response.data)
     
  }
  static logOut=()=>{
       
    return  axios.get('http://localhost:8080/api/users/logout',{withCredentials: true})

}
  static getServices=()=>{

      return axios.get('http://localhost:8080/api/services',{withCredentials: true}).then(response=>response.data)
  
  }
  static getOneService=(id)=>{
    return axios.get('http://localhost:8080/api/services/'+id,{withCredentials: true}).then().then(response=>response.data)
  }
  static getOneInvoice=(id)=>{
    return axios.get('http://localhost:8080/api/invoice/'+id,{withCredentials: true}).then().then(response=>response.data)
  }
  static deleteService(id){
    return axios('http://localhost:8080/api/services/'+id,{method:"DELETE",withCredentials: true})
  }
  static deleteInvoice(id){
    return axios('http://localhost:8080/api/invoices/'+id,{method:"DELETE",withCredentials: true})
  }
  static getInvoicesByService(id){
    return axios.get('http://localhost:8080/api/services/'+id+"/invoices",{withCredentials: true}).then(response=>response.data)
  }
  static insertService(body){
    const method="post";
    const url="http://localhost:8080/api/services"
    return this.postOrPut(method,url,body)
  } 

  static insertInvoice(body){
    const method="post";
    const url="http://localhost:8080/api/invoices"
    return this.postOrPut(method,url,body)
  } 

  static updateService(id,body){
    const method="put"
    const url="http://localhost:8080/api/services/"+id
        return this.postOrPut(method,url,body)
  }
  static updateInvoice(id,body){

    const method="put"
    const url="http://localhost:8080/api/invoices/"+id
        return this.postOrPut(method,url,body)
  }
  static login(body){
    return this.postOrPut("post","http://localhost:8080/api/users/login",body);
  }
  static getCurrentUser(){
    return axios.get('http://localhost:8080/api/users/current',{withCredentials: true}).then().then(response=>response.data)
  }
  static updateUser(id,body){
    const method="put"
    const url="http://localhost:8080/api/users/"+id
        return this.postOrPut(method,url,body)
  }
  static register(body){
    return this.postOrPut("post","http://localhost:8080/api/users/register",body);
  }
   static postOrPut(method,url,body){
 
        return axios.request(url,{
        method:method,
        data:JSON.stringify(body),
        mode: 'cors',
        withCredentials:true,
        headers: { 
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'*'
        }
        })
    }
}