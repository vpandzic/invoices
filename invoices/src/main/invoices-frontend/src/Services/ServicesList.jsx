import React, { Component } from 'react';
import ServiceItem from './ServiceItem'
import { Link } from 'react-router-dom'
import dataService from '../dataService';

export default class ServicesList extends Component {

  constructor(props) {
    super(props);
    this.state = {
         services: [],
         isLoading: true
        };
        this.onServiceDelete=this.onServiceDelete.bind(this);
  }
  onServiceDelete(deletedId){
      this.setState({
          services:this.state.services.filter(x=>x.id!=deletedId)
      })
  }
  componentDidMount() {
    this.setState({isLoading: true});

    dataService.getServices()
      .then(data =>{
         
          this.setState({services: data, isLoading: false})
          }
      );
  }
  render(){
        return (<div class="col-md-12">
            <br/>
            <h2>Usluge</h2>
            <hr/>
             <Link to="/services/-1/addEdit" className="btn btn-primary">Dodaj novu uslugu</Link>
            <hr/>
            
                {this.state.services.map((service)=>{
                    return (
                        <div class="list-group">
                            <ServiceItem service={service} onServiceDelete={this.onServiceDelete} />
                       </div>
                    )
                })
                }        
         </div>
        )
  }
}
