import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import dataService from '../dataService';

export default class ServiceItem extends Component {

    constructor(props) {
        super(props)
        this.onServiceDelete = this.onServiceDelete.bind(this);

    }
    onServiceDelete() {

        dataService.deleteService(this.props.service.id).then((response) => {
            if (response.status == 200) {
                this.props.onServiceDelete(this.props.service.id)
            } else {
                alert("Došlo je do greške")
            }
        })
    }
    render() {
        const service = this.props.service;
        return (

            <a className="list-group-item list-group-item-action">
                {service.name}
                <div class="btn-group float-right" role="group" aria-label="Basic example">
                 <Link to={"/services/"+service.id}  params={{ service: service}} className="btn btn-primary">Detaljno</Link>

                    <button type="button" class="btn btn-warning"><Link className="btn btn-sm" to={"services/" + service.id + "/addEdit"}>Uredi</Link></button>
                    <button type="button" class="btn btn-danger" onClick={this.onServiceDelete}>Izbriši</button>


                </div>
            </a>
        )
    }
}
