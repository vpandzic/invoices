import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Formik ,ErrorMessage, Field } from 'formik';
import * as yup from 'yup'
import FormInput from '../Common/FormInput'
import dataService from '../dataService';

export default class AddEditService extends Component {
    
constructor(props){
    super(props)
    this.state={
        service:{
            name:null,
           
        },
        isNewService:this.props.match.params.id=="-1",
        serviceId:this.props.match.params.id,
        isSaving:false,
        saved:false
    }
  
}
componentDidMount(){
    
    if(!this.state.isNewService){
        dataService.getOneService(this.state.serviceId).then((data)=>{
         
            this.setState({
                service:data
            })
        })
        }
}
 handleResponse=(response)=>{
   
         
        if(response.status==201 || response.status==200){
            this.setState({
                saved:true,
                isSaving:false,
                service:{}
            })
        }
        else{
            this.setState({
                error:true,
                isSaving:false
            })
        }  
}
render(){
    const {isNewService}=this.state;
   
    return (<div>
        <br/>
        <h2>{isNewService ? "Unesi novu uslugu ":"Uredi uslugu "}</h2>
       {this.state.saved && <div class="alert alert-success">Uspješno spremljeno</div> }
       {this.state.error && <div class="alert alert-danger">Došlo je do greške</div> }
        <hr/>
     <Formik
     enableReinitialize={true} 
      initialValues={this.state.service}
       onSubmit={(values, actions) => {
           this.setState({
               isSaving:true,
               saved:false,
               error:false
           })
           
           if(this.state.isNewService==false){
               
            dataService.updateService(this.state.service.id,values).then(this.handleResponse)
          
           }else{

             dataService.insertService(values).then(this.handleResponse)
           }
          
      }}
      
      validationSchema={
          yup.object().shape({
                name: yup.string().nullable().required("Ime je obavezno polje"),
                
      })
    }
  
      render={props => (
        <form onSubmit={props.handleSubmit} className="needs-validation">
          
          
           <Field name="name"  render={({field,form}) => (
                <FormInput  label="Naziv:"  name={"name"} field={field} form={form}  />
            )}
          />
          
       
         
          <button type="submit" disabled={this.state.isSaving} className="btn btn-primary btn-large" >Spremi</button>
        </form>
       )}
       />
    </div>)
}

}