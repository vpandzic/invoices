import React, { Component } from 'react';

import { Link } from 'react-router-dom'
import dataService from '../dataService';
import InvoiceItem from '../Invoices/InvoiceItem'

export default class ServiceDetails extends Component {

  constructor(props) {
    super(props);
    this.setState({isLoading: true});
 
    this.state={
        invoices:[]
    }
  }

  componentDidMount() {
    dataService.getInvoicesByService(this.props.match.params.id)
    .then(data =>{
       
        this.setState({invoices: data, isLoading: false})
        }
    );
  }
  render(){
        return (<div class="col-md-12">
            <br/>
          Računi za ovu uslugu:
          <br/>
          {this.state.invoices.length==0 ?"Nema unesenih računa za ovu uslugu" :null}
           <div className="row">
                {this.state.invoices.map((invoice)=>{
                    return (
                        <div className="col-md-3">
                            <InvoiceItem invoice={invoice} />
                      </div>
                    )
                })
                }
           </div>
         </div>
        )
  }
}
