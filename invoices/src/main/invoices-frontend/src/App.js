import React, { Component } from 'react';
import 'bootstrap';

import logo from './logo.svg';
import './App.css';
import axios from 'axios'
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom'
import InvoiceList from './Invoices/InvoiceList'
import ServicesList from './Services/ServicesList'
import InvoiceDetails from './Invoices/InvoiceDetails'
import AddEditInvoice from './Invoices/AddEditInvoice'
import AddEditService from './Services/AddEditService'
 import Register from './Users/Register'
import Login from './Users/Login'
import Navbar from './Common/Navbar'
import Profile from './Users/Profile';
import ServiceDetails from './Services/ServiceDetails';

export default class App extends Component {

  constructor(props)
  {
    super(props);
         
      axios.interceptors.response.use(function (response) {
        // Do something with response data
        return response;
      }, function (error) {
        if(error.response && (error.response.status==401 || error.response.status==403) && window.location.pathname.indexOf("login")==-1){
            window.location.href="http://localhost:3000/login"
        }
        else if( window.location.pathname.indexOf("login")>-1){
        
          return Promise.resolve(error.response);
        }
        else{
          return Promise.reject(error);
        }
      });
  }
  render(){
    return (
      <div>
          <Router>
          {window.location.pathname.includes("login")==false && window.location.pathname.includes("register")==false ?  <Navbar /> :null}
            <div className="container">
                <div className="row">
                <Route key={"invoices"}  path={"/"} component={InvoiceList} exact/>
                <Route key={"profile"}  path={"/profile"} component={Profile} exact/> 
                <Route key={"invoices"}  path={"/invoices"} component={InvoiceList} exact/>
                   <Route key={"register"}  path={"/register"} component={Register} exact/> 
                    <Route key={"login"}  path={"/login"} component={Login} exact/>
                    <Route key={"services"}  path={"/services"} component={ServicesList} exact/> 
                    <Route key={"invoiceId"}  path={"/invoices/:id"} component={InvoiceDetails} exact/> 
                    <Route key={"serviceId"}  path={"/services/:id"} component={ServiceDetails} exact/> 
                    <Route key={"servicesId"}  path={"/services/:id/addEdit"} component={AddEditService} exact/> 
                    <Route key={"invoicesAddEdit"}  path={"/invoices/:id/addEdit"} component={AddEditInvoice} /> 
                  
                </div>
            </div>
        </Router>
      </div>
    )
  }
 
 
}

