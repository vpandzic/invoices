import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import DataService from '../dataService';

export default class Navbar extends Component {

constructor(props){
    super(props);
    this.logout=this.logout.bind(this);
}
logout(){
    DataService.logOut().then(response=>{
      
        if(response.status==200){
            window.location.href="http://localhost:3000/login"
        }
    })
}

render(){

            return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand" href="#">Invoices</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                <Link className="nav-link" to="/invoices">Računi <span className="sr-only">(current)</span></Link> 
                </li>
                <li className="nav-item">
                <Link className="nav-link" to="/services">Usluge</Link>
                </li>
                
                </ul>
                <Link className="nav-link" to="/profile">Korisnik</Link>
                <a className="btn btn-primary" role="button" onClick={this.logout}>Odlogiraj se</a>
            
            </div>
            </nav>
    )
    }
}