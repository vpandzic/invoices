import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Field } from 'formik';

export default class FormInput extends Component {

constructor(props){
    super(props);
    
}

    render(){
        const {field,form,name,label,type}=this.props;
     
        return (
            <div className="form-group row ">
                        <label htmlFor="name" className="col-md-3">{label}</label>
                        <div className="col-md-9">
                            <input 
                            type={type || "text"}
                             className={"form-control " + (form.errors[name] && form.errors[name].length>0 && form.submitCount>0 ? "is-invalid": (form.dirty && form.submitCount>0  ? "is-valid":""))} 
                             {...field}
                             
                             />
                            <div className="invalid-feedback">{form.errors[name]}</div>
                        </div>
                </div>
        )
    }
}