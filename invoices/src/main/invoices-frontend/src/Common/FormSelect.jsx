import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Field } from 'formik';

export default class FormSelect extends Component {

constructor(props){
    super(props);
    
}

    render(){
        const {field,form,name,label,options}=this.props;
    
        return (
            <div className="form-group row ">
                        <label htmlFor={name} className="col-md-3">{label}</label>
                        <div className="col-md-9">
                            <select 
                             className={"form-control " + (form.errors[name] && form.errors[name].length>0 && form.submitCount>0 ? "is-invalid": (form.dirty && form.submitCount>0  ? "is-valid":""))} 
                             {...field}
                             
                             >
                                 {options.map(x=>{
                                     return <option key={x.value} value={x.value}>{x.name}</option>
                                 })
                                }
                             </select>
                            <div className="invalid-feedback">{form.errors[name]}</div>
                        </div>
                </div>
        )
    }
}