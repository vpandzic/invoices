
import React, { Component } from 'react';
import { Formik , Field } from 'formik';
import * as yup from 'yup'
import FormInput from '../Common/FormInput'
import dataService from '../dataService';
import AddEditUser from './AddEditUser';
export default class Register extends Component {

    constructor(props){
        super(props);
       
        this.state={

            user:{

            }
        }
    }

    render(){
      
        return (  <div className="container col-md-12">
        <div className="col-md-6 offset-md-3">
          {this.state.saved && <div className="alert alert-success">Račun kreiran</div>}
          {this.state.error && <div className="alert alert-danger">Došlo je do greške</div>}

          <br/>
           <h2>Registriraj se</h2>
           <hr/>
          <br/>
        
            <AddEditUser onSuccess={()=>this.setState({saved:true})} onError={()=>this.setState({saved:false})} />
           </div>
        </div>)
    }
}
