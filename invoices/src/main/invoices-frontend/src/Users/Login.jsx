
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Formik ,ErrorMessage, Field } from 'formik';
import * as yup from 'yup'
import FormInput from '../Common/FormInput'
import dataService from '../dataService';
export default class Login extends Component {

    constructor(props){
        super(props);
        this.state={
            user:{

            }
        }
    }

    render(){
   
        return (  <div className="container col-md-12">
        <div className="col-md-6 offset-md-3">
          {this.state.saved && <div className="alert alert-success">Uspješno logiran</div>}
          {this.state.error && <div className="alert alert-danger">Dpšlo je do greške</div>}

          <br/>
           <h2>Logiraj se</h2>
           <hr/>
          <br/>
     
         <Formik

           enableReinitialize={true} 
           initialValues={this.state.user}
           onSubmit={(values, actions) => {
               this.setState({
                   isSaving:true,
                   saved:false,
                   error:false
               })
             
            dataService.login(values).then(response=>{
              debugger
                if(response && (response.status==201 || response.status==200)){
                    this.setState({
                        saved:true,
                        isSaving:false,
                    })
                    window.location.href="http://localhost:3000/services"
                }
                else{
                    this.setState({
                        error:true,
                        isSaving:false
                    })
                }
            })
              
            
           
          }}
          
          validationSchema={
              yup.object().shape({
                   
                    email: yup.string().email("Nije valjana email adresa").required("Email je obavezno polje"),
                    password: yup.string().required("Lozinka je obavezno polje")
          })
        }
      
          render={props => (
            <form onSubmit={props.handleSubmit} className="needs-validation">
              
         
                   <Field name="email"  render={({field,form}) => (
                    <FormInput  label="Email:"  name={"email"} field={field} form={form}  />
                )}
              />
                <Field type="password"  name="password"  render={({field,form}) => (
                    <FormInput type="password"  label="Lozinka:"  name={"password"} field={field} form={form}  />
                )}
              />
           
             
              <button type="submit" disabled={this.state.isSaving} className="btn btn-primary btn-large" >Logiraj se</button>
            </form>
             
           )}
           />
           <hr/>
             <Link to="/register">Registriraj se</Link>
           </div>
         
        </div>)
    }
}
