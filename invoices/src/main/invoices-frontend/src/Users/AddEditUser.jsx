
import React, { Component } from 'react';
import { Formik, Field, HiddenField } from 'formik';
import * as yup from 'yup'
import FormInput from '../Common/FormInput'
import dataService from '../dataService';
export default class AddEditUser extends Component {

    constructor(props) {
        super(props);
      
        this.state = {
            user:props.user || {}
        }
    }
    componentWillReceiveProps(props){
        this.state = {
            user:props.user || {}
        }
    }

    render() {

        return (
            <div>

                <Formik

                    enableReinitialize={true}
                    initialValues={this.state.user}
                    onSubmit={(values, actions) => {
                        if (values)
                            this.setState({
                                isSaving: true,
                                saved: false,
                                error: false
                            })
                        if (!!values.id) {
                            dataService.updateUser(values.id,values).then(response=>{
                                if(response.status==200){
                                    this.setState({
                                        saved: true,
                                        isSaving: false,
                                    })
                                    this.props.onSuccess();
                                    
                                }else{
                                    this.setState({
                                        error: true,
                                        isSaving: false
                                    })
                                    this.props.onError();
                                }

                            })
                        } else {
                            dataService.register(values).then(response => {

                                if (response.status == 201 || response.status == 200) {

                                    this.setState({
                                        saved: true,
                                        isSaving: false,
                                    })
                                    this.props.onSuccess();

                                    window.location.href = "http://localhost:3000";
                                }
                                else {
                                    this.setState({
                                        error: true,
                                        isSaving: false
                                    })
                                    this.props.onError();
                                }
                            })
                        }
                    }}

                    validationSchema={
                        yup.object().shape({
                            id: yup.string(),
                            name: yup.string().required("Ime je obavezno polje"),
                            surname: yup.string().required("Prezime je obavezno polje"),
                            email: yup.string().email("Nije valjana email adresa").required("Email je obavezno polje"),
                            password: yup.string().required("Lozinka je obavezno polje"),
                            phone: yup.string().required("Telefon je obavezno polje"),
                            address: yup.string().required("Adresa je obavezno polje")

                        })
                    }

                    render={props => (
                        <form onSubmit={props.handleSubmit} className="needs-validation">
                            <div style={{display:"none"}}>
                                <Field name="id" render={({ field, form }) => (
                                    <FormInput type="hidden" label="Id:" name={"id"} field={field} form={form} />
                                )}
                                />
                            </div>
                            <Field name="name" render={({ field, form }) => (
                                <FormInput label="Ime:" name={"name"} field={field} form={form} />
                            )}
                            />
                            <Field name="surname" render={({ field, form }) => (
                                <FormInput label="Prezime:" name={"surname"} field={field} form={form} />
                            )}
                            />
                            <Field name="email" render={({ field, form }) => (
                                <FormInput label="Email:" name={"email"} field={field} form={form} />
                            )}
                            />
                            <Field name="phone" render={({ field, form }) => (
                                <FormInput label="Telefon:" name={"phone"} field={field} form={form} />
                            )}
                            />
                            <Field name="address" render={({ field, form }) => (
                                <FormInput label="Adresa:" name={"address"} field={field} form={form} />
                            )}
                            />
                            <Field name="password" type="password" render={({ field, form }) => (
                                <FormInput label="Lozinka:" type={"password"} name={"password"} field={field} form={form} />
                            )}
                            />


                            <button type="submit" disabled={this.state.isSaving} className="btn btn-primary btn-large" >Spremi</button>
                        </form>
                    )}
                />
            </div>
        )
    }
}
